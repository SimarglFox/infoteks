#ifndef SEARCHCLASS_H
#define SEARCHCLASS_H

#include <QObject>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQuickView>

class searchClass : public QObject
{
    Q_OBJECT
public:
    explicit searchClass(QObject *viewer);

signals:

public slots:
    void buttonClicked();

private:
    QObject *rootViewer;

    bool isSorted(QVector<int> &array);
};

#endif // SEARCHCLASS_H
