import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    property alias fileSelectButton: fileSelectButton
    width: 360
    height: 360
    property alias findButton: findButton
    property alias textEditFile: textEditFile

    TextEdit {
        id: textEditFile
        objectName: "textEditFile"
        x: 137
        y: 18
        width: 194
        height: 20
        text: fileUrlformatedText.text
        font.wordSpacing: -3
        font.weight: Font.Normal
        font.pixelSize: 12
        Rectangle {
            id: textEditFileBorder
            x: 0
            y: 0
            width: 199
            height: 20
            color: "#00000000"
            border.width: 1
            border.color: "#000000"
            radius: 0
            opacity: 1
        }
    }
    TextEdit {
        id: searchValue
        objectName: "searchValue"
        x: 137
        y: 63
        width: 199
        height: 20
        text: qsTr("")
        font.pixelSize: 12
        Rectangle {
            id: searchValueBorder
            x: 0
            y: 0
            width: 199
            height: 20
            color: "#00000000"
            border.width: 1
            border.color: "#000000"
            radius: 0
        }
    }

    Text {
        id: text1
        x: 8
        y: 21
        text: qsTr("Файл json с массивом")
        font.pixelSize: 12
    }

    Text {
        id: text2
        x: 8
        y: 66
        text: qsTr("Искомый элемент")
        font.pixelSize: 12
    }
    Button {
        id: fileSelectButton
        x: 342
        y: 18
        width: 86
        height: 20
        text: "Выбрать файл"
    }

    Button {
        id: findButton
        x: 8
        y: 99
        text: "Найти"
    }
}
