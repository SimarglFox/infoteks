#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <searchclass.h>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    searchClass myClass(engine.rootObjects().first());
    engine.rootContext()->setContextProperty("_searchclass", &myClass);
    return app.exec();
}
