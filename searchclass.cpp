#include "searchclass.h"
#include <QQmlContext>

searchClass::searchClass(QObject *viewer) : QObject(viewer)
{
    rootViewer = viewer;
}

void searchClass::buttonClicked()
{
    QObject* filePathEdit = rootViewer->findChild<QObject*>("textEditFile");
    QObject* searchValueEdit = rootViewer->findChild<QObject*>("searchValue");
    QObject* message = rootViewer->findChild<QObject*>("MessageDialog");

    QString filePath = (filePathEdit->property("text")).toString();
    int searchValue = ((searchValueEdit->property("text")).toString()).toInt();

    if(filePath.isEmpty())
    {
        message->setProperty("text", "файл не выбран");
        message->setProperty("visible", true);
        return;
    }
    QFile inputfile(filePath);

    if(!inputfile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        message->setProperty("text", "выбраный файл не существует");
        message->setProperty("visible", true);
        return;
    }

    QByteArray jsonFile = inputfile.readAll();

    QJsonDocument inputDoc(QJsonDocument::fromJson(jsonFile));

    if(!inputDoc.isObject())
    {
        message->setProperty("text", "Этот не соотвествует Json формату");
        message->setProperty("visible", true);
        return;
    }
    QJsonObject inputObject = inputDoc.object();
    QVector<int> array;

    if(inputObject.contains("array") && inputObject["array"].isArray())
    {
        QJsonArray inputArray = inputObject["array"].toArray();
        for(int i = 0; i < inputArray.size(); i++)
        {
            array.push_back(inputArray[i].toInt());
        }
    }

    if(!isSorted(array))
    {
        qSort(array);
    }

    for(int i = 0; i < array.size(); i++)
    {
        if(array[i] == searchValue)
        {
            QString messageText = "Искомое значение находится на " + QString::number(i) + " позиции";
            message->setProperty("text", messageText);
            message->setProperty("visible", true);
            return;
        }
    }
    message->setProperty("text", "Значение не найдено");
    message->setProperty("visible", true);

}

bool searchClass::isSorted(QVector<int> &array)
{
    if(array.size() == 1)
    {
        return true;
    }
    for (int i = 1; i < array.size(); i++)
    {
        if(array[i-1] > array[i])
        {
            return false;
        }
    }
    return true;
}
