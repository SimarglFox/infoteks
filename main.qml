import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.0
import QtQuick.Dialogs 1.2
Window {
    visible: true
    width: 450
    height: 150
    title: qsTr("Бинарный поиск")
    MessageDialog {
        id: messageDialog
        objectName: "MessageDialog"
        title: "Message"
        text: ""
        onAccepted: {
            visible = false
        }
    }
    MainForm {
        anchors.fill: parent

        fileSelectButton.onClicked: {
            fileDialog.open();
        }

        findButton.onClicked: {
            _searchclass.buttonClicked();
        }

        Text
        {
            id: fileUrlformatedText
            text: fileDialog.fileUrl.toString().replace('file:///', '')
            visible: false
        }

        FileDialog {
            id: fileDialog
            title: "выберите файл"
            selectMultiple: false
            selectFolder: false
            selectExisting: true
            folder: shortcuts.home
            //Component.onCompleted: visible = true
        }
    }

}
